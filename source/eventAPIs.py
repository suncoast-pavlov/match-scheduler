#!/usr/bin/python3
###########################
# Match Scheduler APIs
###########################

from flask import Flask, request
import json
import mariadb
import sys
from pavlov import PavlovRCON
import asyncio
import requests
import time

path = sys.path[0]
x = path.rfind('/')
configPath = path[:x+1]+'config/config.json'
fConfig = open(configPath,)
config = json.load(fConfig)['test1Local']


class Query(): # Class contains all the SQL queries, as well as the mariadb connection and cursor
    
    def __init__(self):
        
        self.conn = mariadb.connect(
            user="matchScheduler",
            password=config['DB_PW'],
            host=config['DB_URL'],
            port=config['DB_PORT'],
            database="EVENTDB")
        self.c = self.conn.cursor()


    def addPlayerToEvent(self,discordID, eventID, playerStatus):
        sql =  '''INSERT INTO PlayerEvents(discordID, eventID, playerStatus)
                VALUES ('%s', '%s', '%s')''' % (discordID, eventID, playerStatus)
        
        self.c.execute(sql)
        self.conn.commit()
        
        return self.c

    def addEvent(self,organiser, mode, timeStart, timeEnd, eventJSON,serverID,recurring):
        sql =  '''INSERT INTO Events(organiser, mode, timeStart, timeEnd, eventJSON,serverID,recurring)
                VALUES ('%s', '%s', '%s', '%s', '%s','%s',%s)''' % (organiser, mode, timeStart, timeEnd, eventJSON,serverID,recurring)
        
        self.c.execute(sql)
        self.conn.commit()
        
        return self.c

    def getEventID(self):
        sql =  '''SELECT MAX(eventID) FROM Events'''
        
        self.c.execute(sql)
        return self.c

    def getAdminsInEvent(self,eventID):
        sql =  '''SELECT oculusName
                FROM PlayerEvents, REGDB.REGISTERED_USERS
                WHERE  REGDB.REGISTERED_USERS.DiscordId = PlayerEvents.discordID 
                        AND PlayerEvents.eventID = %s
                        AND  playerStatus = 'admin\'''' % eventID
        
        self.c.execute(sql)
        return self.c

    def getEventJSON(self,eventID):
        sql =  '''SELECT eventJSON FROM Events WHERE eventID=%s''' % eventID
        
        self.c.execute(sql)
        return self.c

    def getMapsForMode(self,modeIntended):
        sql =  '''SELECT resourceID, contentName
                FROM UGC
                WHERE contentType = 'map' 
                AND (modeIntended = '%s' OR modeIntended = 'all')''' % modeIntended
        
        self.c.execute(sql)
        return self.c

    def getMaps(self):
        sql =  '''SELECT resourceID, contentName
                FROM UGC
                WHERE contentType = "map" AND modeIntended <> "CUSTOM"'''
        
        self.c.execute(sql)
        return self.c

    def getMods(self):
        sql =  '''SELECT resourceID, contentName
                FROM UGC
                WHERE contentType = "mod"'''
        
        self.c.execute(sql)
        return self.c

    def getModes(self):
        sql =  '''SELECT resourceID, contentName, contentType
                FROM UGC
                WHERE contentType = "gamemode" or modeIntended = "CUSTOM"'''
        
        self.c.execute(sql)
        return self.c
    
    def getUGCname(self,resourceID):
        sql = '''SELECT contentName
                 FROM UGC
                 WHERE resourceID = "%s"'''%resourceID
        
        self.c.execute(sql)
        return self.c


    def updateEventWithInfo(self,organiser, mode, timeStart, timeEnd, eventJSON, serverID, eventID):
        sql =  '''UPDATE Events
                SET organiser = '%s', mode = '%s', timeStart = '%s', timeEnd = '%s', eventJSON = '%s', serverID = %s
                WHERE eventID = %s''' % (organiser, mode, timeStart, timeEnd, eventJSON, serverID, eventID)
        
        self.c.execute(sql)
        self.conn.commit()
        
        return self.c
    '''
    def updateEventMap(self,resourceID, eventID):
        sql =  \'''UPDATE UGCEvents
                SET resourceID = '%s'
                WHERE eventID = %s\''' % (resourceID, eventID)
        
        self.c.execute(sql)
        self.conn.commit()
        
        return self.c'''
    
    def getEventsFilter(self,timeStart,timeEnd,gamemode):
        sql = '''SELECT eventJSON
                FROM Events
                WHERE deleted = 0 AND timeStart BETWEEN %s AND %s %s
                ORDER BY timeStart ASC''' % (timeStart, timeEnd,gamemode)
                
        
        self.c.execute(sql)
        
        return self.c
    
    def updateEventJSON(self,event):
        sql = '''UPDATE Events
                SET eventJSON = '%s'
                WHERE eventID = %s'''% (json.dumps(event),event['ID'])
        self.c.execute(sql)
        self.conn.commit()

        return self.c
    
    def isPlayerInEvent(self, eventID,discordID):
        sql = '''SELECT EXISTS(SELECT * FROM PlayerEvents
                               WHERE eventID = %s AND discordID = '%s' AND playerStatus <> 'admin')''' % (eventID,discordID)
        self.c.execute(sql)
        return self.c
    
    def updatePlayerStatus(self, eventID,discordID,playerStatus):
        sql = '''UPDATE PlayerEvents
                 SET playerStatus = '%s'
                 WHERE eventID = %s AND discordID = %s'''%(playerStatus,eventID,discordID)
        self.c.execute(sql)
        self.conn.commit()

        return self.c
    
    def removePlayerFromEvent(self, eventID,discordID):
        sql = 'DELETE FROM PlayerEvents WHERE eventID = %s AND discordID = "%s"' % (eventID,discordID)
        self.c.execute(sql)
        self.conn.commit()
        return self.c
    
    def addMap(self,mapName,mapUGC,modeIntended):
        sql = '''INSERT INTO UGC(resourceID,contentName,contentType,modeIntended)
                 VALUES ('%s', '%s', 'map','%s')''' % (mapUGC,mapName,modeIntended)
        self.c.execute(sql)
        self.conn.commit()
        return self.c
    
    def addMod(self,modName,modUGC):
        sql = '''INSERT INTO UGC(resourceID,contentName,contentType)
                 VALUES ('%s', '%s', 'mod')''' % (modUGC,modName)
        self.c.execute(sql)
        self.conn.commit()
        return self.c
    
    def addMode(self,modeName,modeUGC):
        sql = '''INSERT INTO UGC(resourceID,contentName,contentType)
                 VALUES ('%s', '%s', 'gamemode')''' % (modeUGC,modeName)
        self.c.execute(sql)
        self.conn.commit()
        return self.c
    
    def addServer(self,ip,rconPort,rconPw,region,mode,name):
        sql = '''INSERT INTO Servers(ip, rconPort, rconPassword, region, mode,serverName)
                 VALUES ('%s','%s','%s','%s','%s','%s')'''%(ip,rconPort,rconPw,region,mode,name)
        self.c.execute(sql)
        self.conn.commit()

        return self.c
    
    def getEventsJoined(self,discordID):
        sql = '''SELECT eventJSON
                 FROM Events, PlayerEvents
                 WHERE Events.eventID = PlayerEvents.eventID AND Events.deleted = 0 AND PlayerEvents.discordID = '%s\'''' % discordID
        self.c.execute(sql)
        return self.c
    
    def getMyEvents(self,discordID):
        sql = '''SELECT eventJSON
                 FROM Events
                 WHERE organiser = '%s' AND deleted = 0''' % discordID
        self.c.execute(sql)
        return self.c
    
    def deleteEvent(self,eventID):
        sql = '''UPDATE Events
                 SET deleted = 1
                 WHERE Events.eventID = %s'''%eventID
        self.c.execute(sql)
        self.conn.commit()
        return self.c
    
    def isDeleted(self,eventID):
        sql = '''SELECT EXISTS(SELECT * 
                               FROM Events 
                               WHERE deleted = 0 AND eventID = %s)''' % eventID
        self.c.execute(sql)
        return self.c
    
    def getEventsToStart(self,time):
        sql = '''SELECT eventJSON
                 FROM Events
                 WHERE eventStarted = 0 AND deleted = 0 AND timeStart < %s'''%time
        self.c.execute(sql)
        return self.c

    def startEvents(self,time):
        sql = '''UPDATE Events
                 SET eventStarted = 1
                 WHERE timeStart < %s AND deleted = 0'''%time
        self.c.execute(sql)
        self.conn.commit()
        return self.c

    def getEventsToEnd(self,time):
        sql = '''SELECT eventJSON
                 FROM Events
                 WHERE eventStarted = 1 AND deleted = 0 AND timeEnd < %s'''%time
        
        self.c.execute(sql)
        return self.c
    
    def endEvents(self,time):
        sql = '''UPDATE Events
                 SET deleted = 1
                 WHERE timeEnd < %s AND deleted = 0 AND eventStarted = 1'''%time
        self.c.execute(sql)
        self.conn.commit()
        return self.c
    
    def findRecurring(self):
        sql ='''SELECT eventJSON
                FROM Events
                WHERE eventStarted = 1 AND deleted = 1 AND recurring = 1
                '''
        self.c.execute(sql)
        return self.c

    def resetRecurring(self):
        sql ='''UPDATE Events
                SET eventStarted = 0, deleted = 0, timeStart = timeStart + 86400*7, timeEnd = timeEnd + 86400*7
                WHERE eventStarted = 1 AND deleted = 1 AND recurring = 1
        '''
        self.c.execute(sql)
        self.conn.commit()
        return self.c
    
    def removeJoinedFromRecurring(self):
        sql ='''DELETE FROM PlayerEvents
                WHERE playerStatus = 'joined'
                AND eventID IN (
                    SELECT eventID
                    FROM Events
                    WHERE eventStarted = 1 AND deleted = 1 AND recurring = 1
                )
        '''
        self.c.execute(sql)
        self.conn.commit()
        return self.c

    def numEvents(self,organiser):
        sql = '''SELECT COUNT(*)
                 FROM Events
                 WHERE organiser = '%s' AND eventStarted = 0 AND deleted = 0'''%organiser
        self.c.execute(sql)
        return self.c
    
    def allocateServer(self,region,mode,timeStart,timeEnd):
        sql= '''SELECT Y.serverID, Y.serverName
                FROM (
                    
                    SELECT serverID FROM Events
                    WHERE deleted = 0
                    AND ((timeStart <= %s AND timeEnd >= %s) OR (timeStart <= %s AND timeEnd >= %s) OR (timeStart >= %s AND timeEnd <= %s))
                    
                ) AS X
                RIGHT JOIN (
                    SELECT serverID, serverName FROM Servers WHERE region = '%s' AND mode = '%s'
                ) AS Y ON X.serverID = Y.serverID
                WHERE X.serverID IS NULL
                LIMIT 1'''%(timeStart,timeStart,timeEnd,timeEnd,timeStart,timeEnd,region,mode)
        
        self.c.execute(sql)
        return self.c
    
    def allocateServerByRegion(self,region,timeStart,timeEnd):
        sql= '''SELECT Y.serverID, Y.serverName
                FROM (
                    SELECT serverID FROM Events
                    WHERE deleted = 0
                    AND ((timeStart <= %s AND timeEnd >= %s) OR (timeStart <= %s AND timeEnd >= %s) OR (timeStart >= %s AND timeEnd <= %s))
                    
                ) AS X
                RIGHT JOIN (
                    SELECT serverID, serverName FROM Servers WHERE region = '%s'
                ) AS Y ON X.serverID = Y.serverID
                WHERE X.serverID IS NULL
                LIMIT 1'''%(timeStart,timeStart,timeEnd,timeEnd,timeStart,timeEnd,region)
        
        self.c.execute(sql)
        return self.c
    
    def checkOverlap(self,timeStart,timeEnd,serverID,eventID):
        sql ='''
            SELECT EXISTS(
                SELECT * FROM Events
                WHERE deleted = 0 AND serverID = %s AND eventID <> %s
                AND ((timeStart <= %s AND timeEnd >= %s) OR (timeStart <= %s AND timeEnd >= %s) OR (timeStart >= %s AND timeEnd <= %s))
            )
        '''%(serverID,eventID,timeStart,timeStart,timeEnd,timeEnd,timeStart,timeEnd)

        self.c.execute(sql)
        return self.c

    def getServerDetails(self,eventID):
        sql= '''SELECT ip, Servers.rconPort, rconPassword, serverName, Servers.mode, region, Servers.serverID
                FROM Events, Servers
                WHERE Events.serverID = Servers.serverID AND eventID = %s'''%eventID
        
        self.c.execute(sql)
        return self.c
    
    def getServersFilter(self,mode,region):
        sql= '''SELECT serverName, mode, region, serverID
                FROM Servers
                WHERE TRUE %s %s
                ORDER BY serverID ASC'''%(mode,region)
        
        self.c.execute(sql)
        return self.c
    
    def calendarDay(self,timeStart,serverID):
        sql= '''
            SELECT timeStart, timeEnd
            FROM Events
            WHERE timeStart BETWEEN %s AND %s
            AND ServerID = %s AND deleted = 0
        '''%(timeStart,timeStart+86399,serverID)

        self.c.execute(sql)
        return self.c


def addListsToDB(event:dict) -> bool: # Adds/updates values in the junction table
    
    try:
        for player in event['admins']:
            db.updatePlayerStatus(event['ID'],player,'admin')
    except:
        pass

    try:            
        for player in event['invites']:
            if bool(db.isPlayerInEvent(event['ID'],player).fetchall()[0][0]):
                db.updatePlayerStatus(event['ID'],player,'invited')
            else:
                db.addPlayerToEvent(player,event["ID"],'invited')
    except:
        pass
    try:
        for player in event['joined']:
            if not bool(db.isPlayerInEvent(event['ID'],player).fetchall()[0][0]):
                db.addPlayerToEvent(player,event["ID"],'joined')
    except:
        pass



async def startEvent(event:dict): # Performs all the rcon commands necessary to start an event
    print('starting event ',event['ID'],flush=True)
    rconDetails = db.getServerDetails(event['ID']).fetchall()[0]
    
    rcon = PavlovRCON(rconDetails[0],rconDetails[1],rconDetails[2])
    
    admins = db.getAdminsInEvent(event['ID']).fetchall()
    for admin in admins:
        print(await rcon.send(f'AddMod {admin[0]}'),flush=True)
        
    for mod in event['mods']:
        print(await rcon.send(f'UGCAddMod {mod}'),flush=True)

    for ban in event['bans']:
        print(await rcon.send(f'ban {ban}'),flush=True)

    if event['mode'] == event['map']:
        event['mode'] = 'CUSTOM'
    print(await rcon.send(f'SetMaxPlayers {event["maxPlayerCount"]}'),flush=True)
    print(await rcon.send(f'SwitchMap {event["map"]} {event["mode"]}'),flush=True)
    if event['code'] != None:
        print(await rcon.send(f'SetPin {event["code"]}'),flush=True)

def sendWebhook(event:dict):
    serverName = db.getServerDetails(event['ID']).fetchall()[0][3]
    users = ([event['organiser']] + event['invites'] + event['joined'])
    users[0] = '<@'+ users[0]
    users[-1] += '>'
    join = '> <@'
    pingString = join.join(users)
    message = f'Event {event["ID"]} Starting!\nJoin {serverName}\n{pingString}'

    data = {'content':message}
    requests.post(config['WEBHOOK_URL'],json=data)
     

    
async def endEvent(event:dict): # Performs all the rcon commands necessary to end an event 
    print('ending event ', event['ID'],flush=True)
    rconDetails = db.getServerDetails(event['ID']).fetchall()[0]
    
    rcon = PavlovRCON(rconDetails[0],rconDetails[1],rconDetails[2])
    
    admins = db.getAdminsInEvent(event['ID']).fetchall()
    for admin in admins:
        if admin[0] not in ['Catilina','Lag_','zTriplerz','KoolAid_Man_','BluIsBlue']:
            print(await rcon.send(f'RemoveMod {admin[0]}'),flush=True)
        
    for ban in event['bans']:
        print(await rcon.send(f'unban {ban}'),flush=True)
    
    print(await rcon.send('UGCClearModList'),flush=True)
    print(await rcon.send('UGCAddMod UGC3462586'),flush=True) # RCON+
    print(await rcon.send('SetPin'),flush=True)
    if rconDetails[4] == 'PUSH':
        print(await rcon.send('SetMaxPlayers 24'),flush=True)
    elif rconDetails[4] == 'SND':
        print(await rcon.send('SetMaxPlayers 16'),flush=True)
    else:
        print(await rcon.send('SetMaxPlayers 10'),flush=True)
    

    if event['recurring']:
        message = f'?changePost {event["ID"]}'
    else:
        message = f'?deletePost {event["ID"]}'
    data = {'content':message}
    requests.post(config['WEBHOOK_URL'],json=data)

async def addModsLive(mods:list,id:int):
    
    rconDetails = db.getServerDetails(id).fetchall()[0]
    rcon = PavlovRCON(rconDetails[0],rconDetails[1],rconDetails[2])

    for mod in mods:
        print(await rcon.send(f'UGCAddMod {mod}'),flush=True)

async def removeModsLive(mods:list,id:int):
    
    rconDetails = db.getServerDetails(id).fetchall()[0]
    rcon = PavlovRCON(rconDetails[0],rconDetails[1],rconDetails[2])

    for mod in mods:
        print(await rcon.send(f'UGCRemoveMod {mod}'),flush=True)

async def banLive(ban, id):
    rconDetails = db.getServerDetails(id).fetchall()[0]
    rcon = PavlovRCON(rconDetails[0],rconDetails[1],rconDetails[2])
    print(await rcon.send(f'ban {ban[0]}'),flush=True)

async def unbanLive(bans, id):
    rconDetails = db.getServerDetails(id).fetchall()[0]
    rcon = PavlovRCON(rconDetails[0],rconDetails[1],rconDetails[2])

    for ban in bans:
        print(await rcon.send(f'unban {ban}'),flush=True)

def resetRecurring():

    events = db.findRecurring().fetchall()
    for event in events:
        event = json.loads(event[0])
        event['timeStart'] += 86400*7
        event['timeEnd'] += 86400*7
        event['posted'] = False
        event['joined'] = []

        server = db.allocateServer(event['region'],event['mode'],event['timeStart'],event['timeEnd']).fetchall()
        if server == []:
            server = db.allocateServerByRegion(event['region'],event['timeStart'],event['timeEnd']).fetchall()[0]
        else: 
            server = server[0]

        event['serverID'] = server[0]
        event['serverName'] = server[1]
        
        db.updateEventJSON(event)
    db.removeJoinedFromRecurring()
    db.resetRecurring()




app = Flask(__name__)
db = Query()

@app.route('/addNewEvent',methods=['POST'])
def addNewEvent() -> dict: # Adds the given event to the db
    
    event = request.json
    try:
        server = db.allocateServer(event['region'],event['mode'],event['timeStart'],event['timeEnd']).fetchall()
        if server == []:
            server = db.allocateServerByRegion(event['region'],event['timeStart'],event['timeEnd']).fetchall()[0]
        else: 
            server = server[0]

   
        event.update({'serverID':server[0]})
        event.update({'serverName':server[1]})
    except Exception as e:
        print(e,flush=True)
        return {}
    db.addEvent(event['organiser'],event['mode'],event['timeStart'],event['timeEnd'],json.dumps(event),event['serverID'],event['recurring'])
    event['ID']=db.getEventID().fetchall()[0][0]
    db.updateEventJSON(event)
    db.addPlayerToEvent(event['organiser'],event['ID'],'admin')
    
    addListsToDB(event)
    ########################################################
    message = f'?changePost {event["ID"]}'                 #
    data = {'content':message}                             # Remove to stop auto-posting events.
    requests.post(config['WEBHOOK_URL'],json=data)         #
    ########################################################
    return event
    

@app.route('/getEvent/<eventID>') # Returns the event details for the given eventID
def getEvent(eventID:str) -> dict:

    try:
        event = json.loads(db.getEventJSON(int(eventID)).fetchall()[0][0])
    except:
        event = {}
    
    return event

@app.route('/getMapList/<mode>') # Returns every map in the db intended for the given mode
def getMapList(mode:str='all') -> dict:
    
    
    if mode in ['SND','PUSH','KOTH','PH']:
        mapList = db.getMapsForMode(mode).fetchall()
    else:
        mapList = db.getMaps().fetchall()
    
    mapDict = dict()
    for map in mapList:
        mapDict.update({map[1]:map[0]})
    
    return mapDict

@app.route('/getModList') # Returns every mod in the db
def getModList() -> dict:
    
    modList = db.getMods().fetchall()
    modDict = dict()
    for mod in modList:
        modDict.update({mod[1]:mod[0]})
    
    return modDict

@app.route('/getModeList')
def getModeList():
    modeList = db.getModes().fetchall()
    modeDict = dict()
    for mode in modeList:
        modeDict.update({mode[1]:[mode[0],mode[2]]})

    return modeDict

@app.route('/updateEvent/<key>',methods=['POST']) # updates the event with a new value
def updateEvent(key) -> dict:
    
    
    changes = request.json
    updatedEvent = getEvent(changes['ID'])
    
    if key == 'bans':
        if updatedEvent['timeStart'] < time.time() < updatedEvent['timeEnd']:
            asyncio.run(banLive(changes[key],changes['ID']))
    if key == 'mods':
        if updatedEvent['timeStart'] < time.time() < updatedEvent['timeEnd']:
            asyncio.run(addModsLive(changes[key],changes['ID']))
            

    if key == 'admins' or key == 'invites' or key == 'joined' or key == 'mods' or key == 'bans':
        updatedEvent[key] += changes[key] 
    else:
        updatedEvent[key] = changes[key]

    if key == 'region':
        try:
            server = db.allocateServer(updatedEvent['region'],updatedEvent['mode'],updatedEvent['timeStart'],updatedEvent['timeEnd']).fetchall()
            if server == []:
                server = db.allocateServerByRegion(updatedEvent['region'],updatedEvent['timeStart'],updatedEvent['timeEnd']).fetchall()[0]
            else: 
                server = server[0]

            
            serverID = server[0]
            serverName = server[1]
            updatedEvent['serverName'] = serverName
            updatedEvent['serverID'] = serverID
        except Exception as e:
            print(e,flush=True)
            return {}
    
    db.updateEventWithInfo(updatedEvent['organiser'],updatedEvent['mode'],updatedEvent['timeStart'],updatedEvent['timeEnd'],json.dumps(updatedEvent),updatedEvent['serverID'],updatedEvent['ID'])
    
    
    addListsToDB(changes)
    
    return updatedEvent  


@app.route('/removeFromEvent/<key>',methods=['POST']) # For removing items from lists
def removeFromEvent(key) -> dict:
    changes = request.json
    updatedEvent = getEvent(changes['ID'])

    if key == 'bans':
        if updatedEvent['timeStart'] < time.time() < updatedEvent['timeEnd']:
            asyncio.run(unbanLive(changes[key],changes['ID']))
    if key == 'mods':
        if updatedEvent['timeStart'] < time.time() < updatedEvent['timeEnd']:
            asyncio.run(removeModsLive(changes[key],changes['ID']))

    for val in changes[key]:
        for i in range(len(updatedEvent[key])):
            if val == updatedEvent[key][i]:
                updatedEvent[key].pop(i)
                if key == 'invites' or key == 'joined':
                    db.removePlayerFromEvent(changes['ID'],val)
                elif key == 'admins':
                    db.updatePlayerStatus(changes['ID'],val,'invited')
                break
    db.updateEventWithInfo(updatedEvent['organiser'],updatedEvent['mode'],updatedEvent['timeStart'],updatedEvent['timeEnd'],json.dumps(updatedEvent),updatedEvent['serverID'],updatedEvent['ID'])
    event = getEvent(changes['ID'])
    return event

@app.route('/checkOverlap',methods=['POST'])
def checkOverlap():
    event = request.json
    db.getServerDetails(event['ID'])
    serverInfo = db.getServerDetails(event['ID']).fetchall()[0]
    overlap = bool(db.checkOverlap(event['timeStart'],event['timeEnd'],serverInfo[6],event['ID']).fetchall()[0][0])
    return {'overlap':overlap}


@app.route('/getEventsFilter/<startTime>/<endTime>/<gamemode>') # Returns all events matching the given filters
def getEventsFilter(startTime:int,endTime:int,gamemode:str) -> dict:
    if gamemode != 'all':
        gamemode = f'AND mode = "{gamemode}"'
    else:
        gamemode=''
    events = db.getEventsFilter(startTime,endTime,gamemode).fetchall()

    return {'events':events}

@app.route('/deleteEvent/<eventID>') # Deletes the event with the given eventID
def deleteEvent(eventID) -> dict:
    db.deleteEvent(int(eventID))

    return {'deleted':True}

@app.route('/getUGCName/<ugc>') # Returns the name of the mod/map with the given resource ID
def getUGCName(ugc):
    ugcName = db.getUGCname(ugc).fetchall()[0][0]

    return {'name':ugcName}
        
@app.route('/removePlayerFromEvent/<eventID>/<discordID>') # Its in the name.
def removePlayerFromEvent(eventID,discordID):
    db.removePlayerFromEvent(eventID,discordID)
    event = getEvent(int(eventID))
    for i in range(len(event['admins'])):
        if event['admins'][i] == discordID:
            event['admins'].pop(i)
            break
    for i in range(len(event['invites'])):
        if event['invites'][i] == discordID:
            event['invites'].pop(i)
            break
    for i in range(len(event['joined'])):
        if event['joined'][i] == discordID:
            event['joined'].pop(i)
            break
    db.updateEventJSON(event)

    return event

@app.route('/addMap/<mapName>/<mapUGC>/<modeIntended>') # Adds a map to the database
def addMap(mapName,mapUGC,modeIntended):
    db.addMap(mapName,mapUGC,modeIntended)
    return {}

@app.route('/addMod/<modName>/<modUGC>') # Adds a mod to the database
def addMod(modName,modUGC):
    db.addMod(modName,modUGC)
    return {}

@app.route('/addMode/<modeName>/<modeUGC>') # Adds a mode to the database
def addMode(modeName,modeUGC):
    db.addMode(modeName,modeUGC)
    return {}

@app.route('/addServer/<ip>/<rconPort>/<rconPw>/<region>/<mode>/<name>') # Adds a server to the database
def addServer(ip,rconPort,rconPw,region,mode,name):
    db.addServer(ip,rconPort,rconPw,region,mode,name)
    return {}


@app.route('/getEventsJoined/<discordID>') # Returns all events the given person is a part of
def getEventsJoined(discordID):
    events = db.getEventsJoined(discordID).fetchall()
    return {'events':events}

@app.route('/getMyEvents/<discordID>') # Returns all events scheduled by the given person which havent been deleted
def getMyEvents(discordID):
    events = db.getMyEvents(discordID).fetchall()
    return {'events':events}

@app.route('/getServersFilter',methods=['POST'])
def getServersFilter():
    serverFilter = request.json
    if serverFilter['mode'] != '':
        mode = f'AND mode = "{serverFilter["mode"]}"'
    else: mode = ''

    if serverFilter['region'] != '':
        region = f'AND region = "{serverFilter["region"]}"'
    else: region = ''

    serverList = db.getServersFilter(mode,region).fetchall()
    servers = dict()
    for server in serverList:
        if server[1] not in servers:
            servers.update({server[1]:{'EU':[],'US':[],'SA':[],'AP':[]}})
        servers[server[1]][server[2]].append(server[0])

    return servers
    


@app.route('/isDeleted/<eventID>') # returns bool whether or not a given eventID exists and isnt deleted
def isDeleted(eventID):
    eventExists = bool(db.isDeleted(int(eventID)).fetchall()[0][0])
    return {'exists':eventExists}

@app.route('/getNumEvents/<organiser>') # Returns the number of upcoming events organised by the person given
def getNumEvents(organiser):
    numEvents = db.numEvents(organiser).fetchall()[0][0]
    return {'numEvents':numEvents}


@app.route('/checkTime/<time>') # Called by eventLauncher, does everything necessary to start/stop events
def checkTime(time):
    time = int(time)

    eventsToStart = db.getEventsToStart(time).fetchall()
    eventsToEnd = db.getEventsToEnd(time).fetchall()

    for event in eventsToStart:
        event = json.loads(event[0])
        asyncio.run(startEvent(event))
        sendWebhook(event)
    for event in eventsToEnd:
        event = json.loads(event[0])
        asyncio.run(endEvent(event))
        
    db.endEvents(time)
    db.startEvents(time)
    resetRecurring()
    
    return {}

@app.route('/getCalendar/<timeStart>')
def getCalendar(timeStart):
    calendar = dict()
    serverList = db.getServersFilter('','').fetchall()
    for server in serverList:
        calendar.update({server[0]: db.calendarDay(int(timeStart),server[3]).fetchall()})
    return calendar

   
   

@app.route('/refreshConfig')
def refreshConfig():
    global config
    fConfig = open(configPath,)
    config = json.load(fConfig)['test1Local']
    return {}


app.run()