#!/usr/bin/python3
############################
# match scheduling bot
############################
import requests
import json
import discord
from discord.ext import commands
from discord import app_commands, Intents
import datetime
import time
import sys
from discord.ui import Button 
from typing import *
from discord.app_commands import Choice
import CalendarMaker
import pytz

path = sys.path[0]
x = path.rfind('/')
configPath = path[:x+1]+'config/config.json'
fConfig = open(configPath,)
config = json.load(fConfig)['test1Local']

serverID = 777516889030393878

def roundToQuarter(num):
    return round(num * 4) / 4

def createEventEmbed(event:dict) -> discord.Embed:
    embed=discord.Embed(title=f"Event {event['ID']}", color=0x00ff00)
    keyOrder = ['organiser','maxPlayerCount','region','map','mode','mods','code','timeStart','timeEnd','admins','invites','joined','serverName']
    for key in keyOrder:
        if key[:4] == 'time':
            embed.add_field(name=key, value=f'<t:{str(event[key])}:f>', inline=True)
        elif key == 'organiser':
            embed.add_field(name=key, value=f'<@{str(event[key])}>', inline=True) 
        elif key == 'invites' or key == 'admins' or key == 'joined':
            x = '>\n<@'
            msg = f'<@{x.join(event[key])}>'
            if msg == '<@>': msg = 'Nobody'
            embed.add_field(name=key,value=msg,inline=True)
        elif key in ['map','mode']:
            embed.add_field(name=key, value=getUGCName(event[key]), inline=True)
        elif key == 'mods':
            temp = list()
            x = '\n'
            for mod in event['mods']:
                temp.append(getUGCName(mod))
            embed.add_field(name=key, value=x.join(temp), inline=True)
        else:
            embed.add_field(name=key, value=str(event[key]), inline=True)
        
    return embed



def createEvent(discordID:str) -> dict: 
    
    event = {
    'region':None,
    'organiser':discordID,
    'map':None,
    'mode':None,
    'invites':[],
    'maxPlayerCount':None,
    'mods':[], # modifiers, dont get confused
    'admins':[discordID],
    'joined':[],
    'timeStart':None,
    'timeEnd':None,
    'code':None,
    'posted':False,
    'recurring':False,
    'bans':[],
    'ID':None
    }
    return event

def addNewEvent(event:dict) -> int:

    savedEvent = json.loads(requests.post(config['API_URL']+'/addNewEvent',json=event).content)
    
    return savedEvent



async def updateEvent(eventID:int,key:str,value,changeType:str='add') -> dict:
    change = {
        'ID':eventID,
        key:value
    }
    if changeType == 'add':
        updatedEvent = json.loads(requests.post(config['API_URL']+'/updateEvent/%s'%key, json=change).content)
    else:
        updatedEvent = json.loads(requests.post(config['API_URL']+'/removeFromEvent/%s'%key, json=change).content)
    
    await updatePost(updatedEvent)

    return updatedEvent

async def changeModsLive(eventID, key, change, type):
    
    if type == 'add':

        print(requests.post(config['API_URL']+f'/addMods/{eventID}',json=change),flush=True)
    else:
        print(requests.post(config['API_URL']+f'/removeMods/{eventID}',json=change),flush=True)
    return await updateEvent(eventID,key,change,type)
       
            
    
 
def getModeDict():

    modeDict = json.loads(requests.get(config['API_URL']+'/getModeList').content) 

    return modeDict

def getMapDict(mode:str) -> dict:
    
    mapDict = json.loads(requests.get(config['API_URL']+'/getMapList/%s' % mode).content)

    return mapDict

def getModDict() -> dict:

    modDict = json.loads(requests.get(config['API_URL']+'/getModList').content)
    
    return modDict

def getEventsFilter(startTime:int,endTime:int,gamemode:str) -> list:
    events = json.loads(requests.get(config['API_URL']+'/getEventsFilter/%s/%s/%s' % (startTime,endTime,gamemode)).content)['events']
    return events




def getUGCName(ugc:str) -> str:
    name = json.loads(requests.get(config['API_URL']+'/getUGCName/%s' % ugc).content)['name']
    return name      
        
def eventExists(eventID):
    return json.loads(requests.get(config['API_URL']+'/isDeleted/%s'%eventID).content)['exists']

def getEvent(eventID):
    return json.loads(requests.get(config['API_URL']+'/getEvent/%s'%eventID).content)

def hasRole(user,*targetRoles):
    for role in user.roles:
        if role.name in targetRoles: return True
    return False

async def updatePost(event,embed=None):
    if event['posted']:
        if embed == None:
            embed = createEventEmbed(event)
        channel = bot.get_channel(config['EVENT_CHANNEL'])
        msg = await channel.fetch_message(event['posted'])
        await msg.edit(embed=embed)



bot = commands.Bot(command_prefix='?',intents=Intents.all())

# Views/Buttons used for create-event
class create:

    class MenuButton(discord.ui.Button):
        def __init__(self, label, key=None, value=None, event=None,msg=None,view=None,embed=None):
            super().__init__()
            self.label = label
            self.value = value
            self.key = key
            self.event = event
            self.msg = msg
            self.endView = view
            self.embed = embed
        

        async def callback(self, interaction: discord.Interaction):
            if self.event:
                self.event[self.key] = self.value
            await interaction.response.edit_message(content=self.msg,view=self.endView(self.event))

    class ListButton(discord.ui.Button):
        def __init__(self, label, key, value, event):
            super().__init__()
            self.label = label
            self.value = value
            self.key = key
            self.event = event
  
            

        async def callback(self, interaction: discord.Interaction):
            
            self.event[self.key].append(self.value)
            self.view.remove_item(self)
            await interaction.response.edit_message(view=self.view)
            
    class recurringView(discord.ui.View):
        def __init__(self,event):
            super().__init__(timeout=None)

            self.add_item(create.MenuButton('No','recurring',False,event,'Please select a region',create.regionView))
            self.add_item(create.MenuButton('Yes','recurring',True,event,'Please select a region',create.regionView))

    class regionView(discord.ui.View):
        def __init__(self,event):
            super().__init__(timeout=None)
          
            self.add_item(create.MenuButton('EU','region','EU',event,'Please pick a mode',create.ModeView))
            self.add_item(create.MenuButton('US','region','US',event,'Please pick a mode',create.ModeView))
            self.add_item(create.MenuButton('AP','region','AP',event,'Please pick a mode',create.ModeView))
            self.add_item(create.MenuButton('SA','region','SA',event,'Please pick a mode',create.ModeView))

    class ModeView(discord.ui.View):

        

        def __init__(self,event):
            super().__init__(timeout=None)
            
            modes = getModeDict()

            for mode in modes:
                if modes[mode][1] == 'gamemode':
                    self.add_item(create.MenuButton(mode,'mode',modes[mode][0],event,'Please pick a map',create.MapView))
                else:
                    self.add_item(create.MenuButton(mode,'mode',modes[mode][0],event,'Please pick what mods you want',create.ModView))
        
        @staticmethod
        def refreshModes():
            create.ModeView.modes = getModeDict()

    class MapView(discord.ui.View):


        def __init__(self,event):
            super().__init__(timeout=None)

            maps = getMapDict(event['mode'])

            for map in maps:
                self.add_item(create.MenuButton(map,'map',maps[map],event,'Please pick what mods you want',create.ModView))
        
        

    class ModView(discord.ui.View):

        def __init__(self,event):
            super().__init__(timeout=None)
                  
            self.event = event
            mods = getModDict()
            for mod in mods:
                self.add_item(create.ListButton(mod,'mods',mods[mod],event))

        @discord.ui.button(label='Done',style=discord.ButtonStyle.green)
        async def button1(self,interaction:discord.Interaction,button:discord.ui.Button):
            
            if self.event['map'] == None:
                self.event['map'] = self.event['mode']

            
            addedEvent = addNewEvent(self.event)
            if addedEvent == {}:
                await interaction.response.edit_message(content=f'Sorry, there are no {self.event["region"]} servers available at that time',view=None)
            else:
                embed = createEventEmbed(addedEvent)
                await interaction.response.edit_message(content='You can invite players to your event with /invite-to-event, edit it with /edit-event, and post it with /post-event',embed=embed,view=None)

# Views/Buttons used for edit-event        
class edit:

    class MenuButton(discord.ui.Button):
        def __init__(self, label, eventID=None, key=None, value=None, msg=None,view=None,embed=None,style=discord.ButtonStyle.gray):
            super().__init__(label=label,style=style)
            
            self.eventID = eventID
            self.value = value
            self.key = key
            self.msg = msg
            self.endView = view
            self.embed = embed
            

        async def callback(self, interaction: discord.Interaction):
          
            if self.eventID:
                self.embed = createEventEmbed(await updateEvent(self.eventID,self.key,self.value))
            await interaction.response.edit_message(content=self.msg,view=self.endView,embed=self.embed)

    class ListButton(discord.ui.Button):
        def __init__(self, label, value, change,style=discord.ButtonStyle.gray):
            super().__init__(label=label,style=style)
            
            self.value = value
            self.change = change
            
           
            

        async def callback(self, interaction: discord.Interaction):
            
            self.change.append(self.value)
            self.view.remove_item(self)
            await interaction.response.edit_message(view=self.view)
            

    class fieldSelect(discord.ui.View):
        def __init__(self,event):
            super().__init__(timeout=None)
            if not(event['posted']):
                self.add_item(edit.MenuButton('region',msg='Please select a region',view=edit.regionView(event)))
            self.add_item(edit.MenuButton('mode',msg='Please select a mode',view=edit.modeView(event)))
            self.add_item(edit.MenuButton('map',msg='Please select a map',view=edit.mapView(event)))
            self.add_item(edit.MenuButton('mods',msg='Would you like to add or remove mods?',view=edit.addRemoveView(edit.modView,'Pick the mods you want to ',event)))
            self.add_item(edit.MenuButton('maxPlayerCount',msg=f'Current maxPlayerCount: {event["maxPlayerCount"]}',view=edit.maxPlayerCountView(event)))
            if time.time() < event['timeStart']:
                self.add_item(edit.MenuButton('timeStart',msg=f'Current timeStart: <t:{event["timeStart"]}:f>',view=edit.timeView(event,'timeStart')))
            self.add_item(edit.MenuButton('timeEnd',msg=f'Current timeEnd: <t:{event["timeEnd"]}:f>',view=edit.timeView(event,'timeEnd')))
            self.add_item(edit.MenuButton('organiser',msg='Please select an admin to replace you',view=edit.organiserView(event)))
            self.add_item(edit.MenuButton('admins',msg='Would you like to add or remove admins ',view=edit.addRemoveView(edit.adminView,'Pick the admins you want to ',event)))
            self.add_item(edit.MenuButton('invites',msg='Who would you like to remove',view=edit.inviteView(event)))
            self.add_item(edit.MenuButton('kick',msg='Who would you like to kick',view=edit.joinedView(event)))
            self.add_item(edit.MenuButton('unban',msg='Who would you like to unban',view=edit.unbanView(event)))
    
    class addRemoveView(discord.ui.View):
        def __init__(self,view,msg,event):
            super().__init__(timeout=None)
            self.add_item(edit.MenuButton('Remove',msg=msg+'remove',view=view(event,'remove'),style=discord.ButtonStyle.red))
            self.add_item(edit.MenuButton('Add',msg=msg+'add',view=view(event,'add'),style=discord.ButtonStyle.green))
    
    class maxPlayerCountView(discord.ui.View):
        def __init__(self,event):
            super().__init__(timeout=None)
            self.event = event
          

        @discord.ui.button(label='-10',style=discord.ButtonStyle.red)
        async def minus60(self,interaction:discord.Interaction,button:discord.ui.Button):
            self.event["maxPlayerCount"] -= 10
            await interaction.response.edit_message(content=f'Current maxPlayerCount: {self.event["maxPlayerCount"]}')

        @discord.ui.button(label='-1',style=discord.ButtonStyle.red)
        async def minus15(self,interaction:discord.Interaction,button:discord.ui.Button):
            self.event["maxPlayerCount"] -= 1
            await interaction.response.edit_message(content=f'Current maxPlayerCount: {self.event["maxPlayerCount"]}')

 #################################################### DONE ##############################################################   
                
        @discord.ui.button(label='Done',style=discord.ButtonStyle.blurple)
        async def done(self,interaction:discord.Interaction,button:discord.ui.Button):

        
            if not(0 < self.event["maxPlayerCount"]<= 24):
                await interaction.response.edit_message(content='Player count must be 1-24',view=None)

            else:
                embed = createEventEmbed(await updateEvent(self.event['ID'],"maxPlayerCount",self.event["maxPlayerCount"]))
                await interaction.response.edit_message(content='maxPlayerCount changed',embed=embed,view=None)

 ########################################################################################################################   
                   
        @discord.ui.button(label='+1',style=discord.ButtonStyle.green)
        async def add15(self,interaction:discord.Interaction,button:discord.ui.Button):
            self.event["maxPlayerCount"] += 1
            await interaction.response.edit_message(content=f'Current maxPlayerCount: {self.event["maxPlayerCount"]}')
        
        @discord.ui.button(label='+10',style=discord.ButtonStyle.green)
        async def add60(self,interaction:discord.Interaction,button:discord.ui.Button):
            self.event["maxPlayerCount"] += 10
            await interaction.response.edit_message(content=f'Current maxPlayerCount: {self.event["maxPlayerCount"]}')
    
    class unbanView(discord.ui.View):
        def __init__(self,event):
            super().__init__(timeout=None)

            self.eventID = event['ID']
            self.change = list()
            

            bans = event['bans']
            for ban in bans:
                self.add_item(edit.ListButton(ban,ban,self.change))
        
        @discord.ui.button(label='Done',style=discord.ButtonStyle.green)
        async def button1(self,interaction:discord.Interaction,button:discord.ui.Button):
            
            embed = createEventEmbed(await updateEvent(self.eventID,'bans',self.change,'remove'))
            await interaction.response.edit_message(content='Bans changed',embed=embed,view=None)

    class regionView(discord.ui.View):
        def __init__(self,event):
            super().__init__(timeout=None)
            self.add_item(edit.MenuButton('EU',event['ID'],'region','EU','Region changed'))
            self.add_item(edit.MenuButton('US',event['ID'],'region','US','Region changed'))
            self.add_item(edit.MenuButton('SA',event['ID'],'region','SA','Region changed'))
            self.add_item(edit.MenuButton('AP',event['ID'],'region','AP','Region changed'))
    
    class modeView(discord.ui.View):
        def __init__(self,event):
            super().__init__(timeout=None)
            modes = getModeDict()
            for mode in modes:
                self.add_item(edit.MenuButton(mode,event['ID'],'mode',modes[mode][0],'Mode changed'))

    
    class mapView(discord.ui.View):
        def __init__(self,event):
            super().__init__(timeout=None)
            maps = getMapDict(event['mode'])
            for map in maps:
                self.add_item(edit.MenuButton(map,event['ID'],'map',maps[map],'Map changed'))

    class modView(discord.ui.View):
        def __init__(self,event,type):
            super().__init__(timeout=None)
            self.eventID = event['ID']
            self.change = list()
            self.type = type
            if type == 'add':
                mods = getModDict()
                temp = mods.copy()
                for mod in temp:
                    if mods[mod] in event['mods']:
                        mods.pop(mod)

                for mod in mods:
                    self.add_item(edit.ListButton(mod,mods[mod],self.change))
            else:
                mods = event['mods']
                for mod in mods:
                    self.add_item(edit.ListButton(getUGCName(mod),mod,self.change))
        
        @discord.ui.button(label='Done',style=discord.ButtonStyle.green)
        async def button1(self,interaction:discord.Interaction,button:discord.ui.Button):
            
            embed = createEventEmbed(await updateEvent(self.eventID,'mods',self.change,self.type))
            await interaction.response.edit_message(content='Mods changed',embed=embed,view=None)
    
    class timeView(discord.ui.View):
        def __init__(self,event,key):
            super().__init__(timeout=None)
            self.event = event
            self.key = key

        @discord.ui.button(label='-1h',style=discord.ButtonStyle.red)
        async def minus60(self,interaction:discord.Interaction,button:discord.ui.Button):
            self.event[self.key] -= 3600
            await interaction.response.edit_message(content=f'Current {self.key}: <t:{self.event[self.key]}:f>')

        @discord.ui.button(label='-15m',style=discord.ButtonStyle.red)
        async def minus15(self,interaction:discord.Interaction,button:discord.ui.Button):
            self.event[self.key] -= 900
            await interaction.response.edit_message(content=f'Current {self.key}: <t:{self.event[self.key]}:f>')

 #################################################### DONE ##############################################################   
                
        @discord.ui.button(label='Done',style=discord.ButtonStyle.blurple)
        async def done(self,interaction:discord.Interaction,button:discord.ui.Button):

        
            if not(0 < self.event['timeEnd'] - self.event['timeStart'] <= 21600):
                await interaction.response.edit_message(content='Your event must be between 0 and 6 hours',view=None)
            elif json.loads(requests.post(config['API_URL']+'/checkOverlap',json=self.event).content)['overlap']:
                await interaction.response.edit_message(content='Sorry, someone else is using this server then',view=None)
            else:
                embed = createEventEmbed(await updateEvent(self.event['ID'],self.key,self.event[self.key]))
                await interaction.response.edit_message(content=f'{self.key} changed',embed=embed,view=None)

 ########################################################################################################################   
                   
        @discord.ui.button(label='+15m',style=discord.ButtonStyle.green)
        async def add15(self,interaction:discord.Interaction,button:discord.ui.Button):
            self.event[self.key] += 900
            await interaction.response.edit_message(content=f'Current {self.key}: <t:{self.event[self.key]}:f>')
        
        @discord.ui.button(label='+1h',style=discord.ButtonStyle.green)
        async def add60(self,interaction:discord.Interaction,button:discord.ui.Button):
            self.event[self.key] += 3600
            await interaction.response.edit_message(content=f'Current {self.key}: <t:{self.event[self.key]}:f>')

    class adminView(discord.ui.View):
        def __init__(self,event,type):
            super().__init__(timeout=None)
            self.change = list()
            self.type = type
            self.event = event

            if type == 'add':
                users = list(set(event['invites'] + event['joined']))
                for user in users:
                    name = bot.get_user(int(user)).name
                    self.add_item(edit.ListButton(name,user,self.change))
            else:
                users = event['admins']
                for user in users:
                    if user != event['organiser']:
                        name = bot.get_user(int(user)).name
                        self.add_item(edit.ListButton(name,user,self.change))

                

        @discord.ui.button(label='Done',style=discord.ButtonStyle.green)
        async def button1(self,interaction:discord.Interaction,button:discord.ui.Button):
            
            if self.type == 'add' and len(self.change) + len(self.event['admins']) > 3:
                await interaction.response.edit_message(content='There can be no more than 3 admins in your event',view=None,embed=None)
            else:
                embed = createEventEmbed(await updateEvent(self.event['ID'],'admins',self.change,self.type))
                await interaction.response.edit_message(content='Admins changed',embed=embed,view=None)



    class inviteView(discord.ui.View):
        def __init__(self,event):
            super().__init__(timeout=None)

            self.eventID = event['ID']
            self.change = list()
           
            users = event['invites']
            for user in users:
                if user != event['organiser']:
                    name = bot.get_user(int(user)).name
                    self.add_item(edit.ListButton(name,user,self.change))


        @discord.ui.button(label='Done',style=discord.ButtonStyle.green)
        async def button1(self,interaction:discord.Interaction,button:discord.ui.Button):
            
            embed = createEventEmbed(await updateEvent(self.eventID,'invites',self.change))
            await interaction.response.edit_message(content='Invites changed',embed=embed,view=None)

    class joinedView(discord.ui.View):
        def __init__(self,event):
            super().__init__(timeout=None)

            self.eventID = event['ID']
            self.change = list()
            

            users = event['joined']
            for user in users:
                if user != event['organiser']:
                    name = bot.get_user(int(user)).name
                    self.add_item(edit.ListButton(name,user,self.change))


        @discord.ui.button(label='Done',style=discord.ButtonStyle.green)
        async def button1(self,interaction:discord.Interaction,button:discord.ui.Button):
            
            embed = createEventEmbed(await updateEvent(self.eventID,'joined',self.change,'remove'))
            await interaction.response.edit_message(content='Joined changed',embed=embed,view=None)

    class organiserView(discord.ui.View):
        def __init__(self,event):
            super().__init__(timeout=None)
            self.change = list()

            users = event['admins']
            for user in users:
                if user != event['organiser']:
                    name = bot.get_user(int(user)).name
                    self.add_item(edit.MenuButton(name,event['ID'],'organiser',user,'Organiser changed'))

        @discord.ui.button(label='Cancel',style=discord.ButtonStyle.red)
        async def button1(self,interaction:discord.Interaction,button:discord.ui.Button):
            
            await interaction.response.edit_message(content='Edit cancelled',embed=None,view=None)
            postedEvent.leave = True
# View used for change-mods-live



class postedEvent(discord.ui.View):
    
    leave = False
    
    def __init__(self,eventID,msg=None):
        super().__init__(timeout=None)
        self.eventID = eventID
        self.msg = msg
        
    
    @discord.ui.button(label='Leave',style=discord.ButtonStyle.red)
    async def button1(self,interaction:discord.Interaction,button:discord.ui.Button):
        event = getEvent(self.eventID)
        if str(interaction.user.id) == event['organiser']:

            self.leave = False
            
            await interaction.response.send_message('Pick an admin to replace you',ephemeral=True,view=edit.organiserView(event))
           
            if self.leave:
                event = json.loads(requests.get(config['API_URL']+'/removePlayerFromEvent/%s/%s' % (event['ID'],str(interaction.user.id))).content)
                
                embed = createEventEmbed(event)
                await self.msg.edit(content='',embed=embed,view=postedEvent(event['ID'],self.msg))
        else:
            await interaction.response.defer()
            event = json.loads(requests.get(config['API_URL']+'/removePlayerFromEvent/%s/%s' % (event['ID'],str(interaction.user.id))).content)

            embed = createEventEmbed(event)
            await self.msg.edit(content='',embed=embed,view=postedEvent(event['ID'],self.msg))
                

    @discord.ui.button(label='Join',style=discord.ButtonStyle.green)
    async def button2(self,interaction:discord.Interaction,button:discord.ui.Button):
        event = getEvent(self.eventID)
        
        await interaction.response.defer()
        if str(interaction.user.id) not in event['joined']:
            updatedEvent = await updateEvent(self.eventID,'joined',[str(interaction.user.id)])
            embed = createEventEmbed(updatedEvent)

            await self.msg.edit(content='', view=postedEvent(self.eventID,self.msg),embed=embed)

        
    
@bot.event
async def on_ready():
    try:
        synced = await bot.tree.sync(guild=discord.Object(id=serverID))
        print('done')
    except Exception as e:
        print(e)


@bot.tree.command(name='create-event',guild=discord.Object(id=serverID),description='Create a new event')
@app_commands.describe(timezone="What timezone do you want to use",time_start = 'Format:hh:mm',duration = 'Hours, Max: 6',max_player_count = 'Max player count on the server',date='Format: yyyy/mm/dd',pin='Password on the server')
@app_commands.choices(timezone=[
        Choice(name='PST', value='US/Pacific'),
        Choice(name='CST', value='US/Central'),
        Choice(name='MST', value='US/Mountain'),
        Choice(name='EST', value='US/Eastern'),
        Choice(name='UTC', value='UTC'),
        Choice(name='GB', value='Europe/London'),
        Choice(name='CET', value='Europe/Paris'),
        Choice(name='AWST', value='Australia/West'),
        ])
async def eventCreator(interaction:discord.Interaction,timezone:Choice[str],time_start:str,duration:int,max_player_count:float,date:Optional[str]=None,pin:Optional[str]=None):
    
    allowed = hasRole(interaction.user,'Mods')
    banned = hasRole(interaction.user,'EventBlacklist')
    
    currentEvents = json.loads(requests.get(config['API_URL']+'/getMyEvents/%s' % str(interaction.user.id)).content)['events']
    
    utcTime = time.mktime(datetime.datetime.now(pytz.timezone('UTC')).timetuple())
    tz = datetime.datetime.now(pytz.timezone(timezone.value)).timetuple()
    tzTime = time.mktime((tz[0],tz[1],tz[2],tz[3],tz[4],tz[5],tz[6],tz[7],0))
    

    hourDif = roundToQuarter((tzTime - utcTime)/3600)

    if date == None:
        date = time.time() + hourDif*3600
        date = datetime.datetime.fromtimestamp(date).timetuple()
    else:
        date = date.split('/')

            

    numEvents = json.loads(requests.get(config['API_URL']+'/getNumEvents/%s'%interaction.user.id).content)['numEvents']
    if banned: await interaction.response.send_message('You have been banned from creating events.')
    elif numEvents > 2 and not(allowed): await interaction.response.send_message('You can only have 3 events scheduled!',ephemeral=True)
    elif not(0 < duration <= 6) and not(allowed): await interaction.response.send_message('Your event must between 1 and 6 hours long',ephemeral=True)
    else:
        event = createEvent(str(interaction.user.id))
 
        timeStart = time_start.split(':')
        dateTime = datetime.datetime(int(date[0]),int(date[1]),int(date[2]),int(timeStart[0]),int(timeStart[1]))
        
        event['timeStart'] = int(time.mktime(dateTime.timetuple()) -hourDif*3600)
        event['timeEnd'] = event['timeStart'] + duration*3600
        
        if event['timeStart'] < time.time():
            await interaction.response.send_message('You cant schedule an event in the past!',ephemeral=True)
            return
        
        if currentEvents != []:                            
            for currentEvent in currentEvents:             
                currentEvent = json.loads(currentEvent[0])
                if (0 < (currentEvent['timeStart'] - event['timeEnd']) < 21600 or 0 < (event['timeStart'] - currentEvent['timeEnd']) < 21600) and not allowed: 
                    await interaction.response.send_message('You must have at least 6 hours between events',ephemeral=True)
                    return
        
        event['maxPlayerCount'] = max_player_count
        if hasRole(interaction.user,'Mods','PrivateEvent'):
            event['code'] = pin

           
        await interaction.response.send_message('Would you like your event to repeat weekly?',view=create.recurringView(event),ephemeral=True)
        return
        



@bot.tree.command(name='invite-to-event',guild=discord.Object(id=serverID),description='Invite people to your event')
@app_commands.describe(event_id='Event ID',invitees='Ping everyone you want to invite')
async def invitePlayers(interaction:discord.Interaction,event_id:int,invitees:str):
    if eventExists(event_id):
        invitees = invitees.replace('><','> <')
        invitedPlayers = invitees.split(' ')
        newPlayers = list()
        event = getEvent(event_id)
        
        if str(interaction.user.id) != event['organiser']:
            await interaction.response.send_message('You dont have permission to do that!',ephemeral=True)

        for player in invitedPlayers:
            if player != '':
                x = player[2:-1]
                if x not in (event['admins'] + event['invites'] + newPlayers):
                    newPlayers.append(x)
                    if 'here' in x or 'everyone' in x:
                        iced = interaction.guild.get_role(783834688783384582)
                        await interaction.user.add_roles(iced)
                        await interaction.response.send_message("This guy tried to ping here/everyone")
                        return
                user = await bot.fetch_user(int(x))
                
        updatedEvent = await updateEvent(event_id,'invites',newPlayers)
        embed = createEventEmbed(updatedEvent)
        await interaction.response.send_message('Event Details:',embed=embed,ephemeral=True)
         
    else:
        await interaction.response.send_message('This event doesnt exist!',ephemeral=True)
    

@bot.tree.command(name='get-events',description="Returns events based on the filters given : No args = All Upcoming Events",guild=discord.Object(id=serverID))
@app_commands.describe(timezone='What timezone are you using',time_start='Find events after: (yyyy/mm/dd-hh:mm)',time_end='Find events before: (yyyy/mm/dd-hh:mm)',gamemode='The mode the event is for')
@app_commands.choices(timezone=[
        Choice(name='PST', value='US/Pacific'),
        Choice(name='CST', value='CST6CDT'),
        Choice(name='MST', value='MST'),
        Choice(name='EST', value='EST'),
        Choice(name='UTC', value='UTC'),
        Choice(name='GB', value='GB'),
        Choice(name='CET', value='CET'),
        Choice(name='AWST', value='Australia/West'),
        ])
async def getEvents(interaction:discord.Interaction, timezone:Optional[Choice[str]]='UTC', time_start:Optional[str]='',time_end:Optional[str]='',gamemode:Optional[str]='all'):

    if timezone == 'UTC':
        hourDif = roundToQuarter((time.mktime(datetime.datetime.now(pytz.timezone(timezone)).timetuple()) - time.mktime(datetime.datetime.now().timetuple()))/3600)
    else:
        hourDif = roundToQuarter((time.mktime(datetime.datetime.now(pytz.timezone(timezone.value)).timetuple()) - time.mktime(datetime.datetime.now().timetuple()))/3600)

    if time_start == '':
        unixTimeStart = int(time.time())
    else:
        
        time_start = time_start.split('-')
        date_start = time_start[0].split('/')
        timeStart = time_start[1].split(':')
        dateTime = datetime.datetime(int(date_start[0]),int(date_start[1]),int(date_start[2]),int(timeStart[0]),int(timeStart[1]))
        unixTimeStart = time.mktime(dateTime.timetuple()) - hourDif*3600
    if time_end == '':
        unixTimeEnd = 2147483647
    else:
        time_end = time_end.split('-')
        date_end = time_end[0].split('/')
        timeEnd = time_end[1].split(':')
        dateTime = datetime.datetime(int(date_end[0]),int(date_end[1]),int(date_end[2]),int(timeEnd[0]),int(timeEnd[1]))
        unixTimeEnd = time.mktime(dateTime.timetuple()) - hourDif*3600
    

    events = getEventsFilter(unixTimeStart,unixTimeEnd,gamemode)
    eventSent = False
    
    channel = bot.get_channel(779448663088431114)

    for event in events:
        event = json.loads(event[0])
        if event['code'] == None:
            embed = createEventEmbed(event)
            postView = postedEvent(event['ID'])
            msg = await channel.send(content='',embed=embed,view=postView)
            postView.msg = msg
            eventSent = True
    if eventSent:
        if interaction.channel == channel:
            await interaction.response.send_message('There you go!')
        else:
            await interaction.response.send_message(f'Responded in <#{channel.id}>',ephemeral=True)
    else:
        await interaction.response.send_message('No events match your search')
            

""" @bot.tree.command(name='post-event',guild=discord.Object(id=serverID),description='Post your event for others to join (you can only do this once)')
@app_commands.describe(event_id='Event ID')
async def postEvent(interaction:discord.Interaction,event_id:int):

    if eventExists(event_id):
        
        event = getEvent(event_id)

        pingRoles = config['PING_ROLES']
        try:
            ping = pingRoles[event['mode']]
        except:
            ping = '<@377204187089338373> needs to create a ping role for this mode'
        
        if str(interaction.user.id) != event['organiser']:
            await interaction.response.send_message('You dont have permission to do that!',ephemeral=True)
            return
        
        if event['posted']: await interaction.response.send_message('You have already posted this event!',ephemeral=True)
        else:
            
            embed = createEventEmbed(event)
            await interaction.response.send_message('',embed=embed,ephemeral=True)
            if event['code'] != None:
                for userID in event['invites']:
                    user = await bot.fetch_user(userID)
                    try:
                        await user.send(f'<@{event["organiser"]}> has invited you to an event!',embed=embed)
                    except:
                        await interaction.channel.send(f'<@{user.id}>, You have been invited to a private event, but must friend me to receive DMs')
                await interaction.user.send('',embed=embed)
            else:
                
                channel = bot.get_channel(config['EVENT_CHANNEL'])
                eventView = postedEvent(event_id)
                eventView.msg = await channel.send(f'<@&{ping}>',embed=embed,view=eventView)
                event = await updateEvent(event['ID'],'posted',eventView.msg.id)
                for userID in event['invites']:
                    msg = await channel.send(f'<@{userID}>')
                    await msg.delete()
                msg = await channel.send(f'<@{interaction.user.id}>')
                
                await msg.delete()
            
    else:
        await interaction.response.send_message('This event doesnt exist!',ephemeral=True) """
    

@bot.tree.command(name='add-map',guild=discord.Object(id=serverID),description='Add a map to the database')
@app_commands.describe(map_name='Name of the map',map_ugc='Resource ID, include the \'UGC\'',mode_intended='What mode is the map for (CUSTOM if mode is included)')
@app_commands.choices(mode_intended=[
        Choice(name='all', value=1),
        Choice(name='SND', value=2),
        Choice(name='PUSH', value=3),
        Choice(name='TDM', value=4),
        Choice(name='DM', value=5),
        Choice(name='CUSTOM', value=6)])
async def addMap(interaction:discord.Interaction,map_name:str,map_ugc:str,mode_intended:Choice[int]):
    canUse = hasRole(interaction.user,'Mods','MapMaker')
    if not canUse:
        await interaction.response.send_message('You dont have permission to do that!',ephemeral=True)
        return
    try:
        json.loads(requests.get(config['API_URL']+'/addMap/%s/%s/%s' % (map_name,map_ugc,mode_intended.name)).content)
        await interaction.response.send_message('Map Added!',ephemeral=True)
    except:
        await interaction.response.send_message('Something went wrong, the map may already have been added',ephemeral=True)

@bot.tree.command(name='add-mod',guild=discord.Object(id=serverID),description='Add a mod to the database')
@app_commands.describe(mod_name='Name of the mod',mod_ugc='Resource ID, include the \'UGC\'')
async def addMod(interaction:discord.Interaction,mod_name:str,mod_ugc:str):
    canUse = hasRole(interaction.user,'Mods','MapMaker')

    if not canUse:
        await interaction.response.send_message('You dont have permission to do that!',ephemeral=True)
        return
    try:
        json.loads(requests.get(config['API_URL']+'/addMod/%s/%s' % (mod_name,mod_ugc)).content)
        await interaction.response.send_message('Mod Added!',ephemeral=True)
    except:
        await interaction.response.send_message('Something went wrong, the mod may already have been added',ephemeral=True)


@bot.tree.command(name='add-mode',guild=discord.Object(id=serverID),description='Add a mode to the database')
@app_commands.describe(mode_name='Name of the mode',mode_ugc='Resource ID, include the \'UGC\'')
async def addMode(interaction:discord.Interaction,mode_name:str,mode_ugc:str):
    canUse = hasRole(interaction.user,'Mods','MapMaker')

    if not canUse:
        await interaction.response.send_message('You dont have permission to do that!',ephemeral=True)
        return
    try:
        json.loads(requests.get(config['API_URL']+'/addMode/%s/%s' % (mode_name,mode_ugc)).content)
        await interaction.response.send_message('Mode Added!',ephemeral=True)
    except:
        await interaction.response.send_message('Something went wrong, the map may already have been added',ephemeral=True)

@bot.tree.command(name='add-server',guild=discord.Object(id=serverID),description='Add a server to the database')
@app_commands.describe(ip='server ip',rcon_port='port used for rcon',rcon_pw='password used for rcon',region='where the server is located',mode='what gamemode is it primarily for',name='what is the server called')
async def addServer(interaction:discord.Interaction,ip:str,rcon_port:str,rcon_pw:str,region:str,mode:str,name:str):
    canUse = hasRole(interaction.user,'Mods')
    if not canUse:
        await interaction.response.send_message('You dont have permission to do that!',ephemeral=True)
        return
    
    mode = mode.upper()
    region = region.upper()

    json.loads(requests.get(config['API_URL']+'/addServer/%s/%s/%s/%s/%s/%s'%(ip,rcon_port,rcon_pw,region,mode,name)).content)
    await interaction.response.send_message('Server Added!',ephemeral=True)

@bot.tree.command(name='get-my-events',guild=discord.Object(id=serverID),description='Lists all upcoming events you\'ve organised')
async def getMyEvents(interaction:discord.Interaction):

    events = json.loads(requests.get(config['API_URL']+'/getMyEvents/%s' % str(interaction.user.id)).content)['events']

    embeds = list()
    for event in events:
        event = json.loads(event[0])
        embeds.append(createEventEmbed(event))
   
    if events == []:
        await interaction.response.send_message('You haven\'t organised any events!')
    else:
        await interaction.response.send_message('Here are your events:',embeds=embeds,ephemeral=True)


@bot.tree.command(name='get-events-joined',guild=discord.Object(id=serverID),description='Lists all upcoming events you\'re in')
async def getEventsJoined(interaction:discord.Interaction):

    events = json.loads(requests.get(config['API_URL']+'/getEventsJoined/%s' % str(interaction.user.id)).content)['events']
    channel = bot.get_channel(config['BOT_CHANNEL'])
    if interaction.channel.id != config['BOT_CHANNEL']:
        await interaction.channel.send(f'Responded in <#{config["BOT_CHANNEL"]}>')
    embeds = list()
    for event in events:
        event = json.loads(event[0])
        if event['code'] == None:
            embed = createEventEmbed(event)
            postView = postedEvent(event['ID']) 
            msg = await channel.send(content='',embed=embed,view=postView)
            postView.msg = msg
        else:
            embeds.append(createEventEmbed(event))
    
    if events == []:
        await interaction.response.send_message('You are not in any events!')
    elif embeds == []:
        await interaction.response.send_message('There you go!')
    else:
        await interaction.response.send_message('These are your private events',embeds=embeds,ephemeral=True)

@bot.tree.command(name='delete-event',guild=discord.Object(id=serverID),description='Delete an event')
@app_commands.describe(event_id='Event ID')
async def deleteEvent(interaction:discord.Interaction,event_id:int):
    if eventExists(event_id):
        event = getEvent(event_id)
        canUse = hasRole(interaction.user,'Mods')

        if event['organiser'] == str(interaction.user.id) or canUse:
            if event['timeStart'] < time.time():
                await interaction.response.send_message('You cannot delete an in progress event.',ephemeral=True)
            json.loads(requests.get(config['API_URL']+'/deleteEvent/%s'%event_id).content)
            if event['posted']:
                channel = bot.get_channel(config['EVENT_CHANNEL'])
                msg = await channel.fetch_message(event['posted'])
                await msg.delete()
            await interaction.response.send_message('Event deleted!',ephemeral=True)
        else:
            await interaction.response.send_message('You dont have permission to do that!',ephemeral=True)
    else:
        await interaction.response.send_message('This event doesnt exist!',ephemeral=True)

@bot.tree.command(name='edit-event',guild=discord.Object(id=serverID),description='Edit an event')
@app_commands.describe(event_id='Event ID')
async def editEvent(interaction:discord.Interaction,event_id:int):
    if eventExists(event_id):
        event = getEvent(event_id)
        if event['organiser'] != str(interaction.user.id):
            await interaction.response.send_message('You dont have permission to do that!',ephemeral=True)
            return
        
        embed = createEventEmbed(event)
        await interaction.response.send_message('What would you like to edit?',view=edit.fieldSelect(event),embed=embed,ephemeral=True)
        


@bot.tree.command(name='ban',guild=discord.Object(id=serverID),description='Ban someone from your event')
@app_commands.describe(event_id='Event ID',oculus_name='Oculus Name')
async def ban(interaction:discord.Interaction,event_id:int,oculus_name:str):
    if eventExists(event_id):
        event = getEvent(event_id)
        if str(interaction.user.id) in event['admins'] +[event['organiser']]:
            event = await updateEvent(event_id,'bans',[oculus_name])
            await interaction.response.send_message(f'{oculus_name} Banned.')
        else:
            await interaction.response.send_message('You dont have permission to ban here')
    else:
        await interaction.response.send_message('This event doesnt exists')    


@bot.tree.command(name='show-servers',guild=discord.Object(id=serverID),description='Show servers by filter')
@app_commands.describe(region='where the server is located',mode='what gamemode is it primarily for')
async def showServers(interaction:discord.Interaction,region:Optional[str]='',mode:Optional[str]=''):
    serverFilter = {'region':region.upper(),
                    'mode':mode.upper()}
    
    servers = json.loads(requests.post(config['API_URL']+'/getServersFilter',json=serverFilter).content)
    embeds = list()
    i = 0
    join = '\n'
    for mode in servers:
        modename = getUGCName(mode)
        embeds.append(discord.Embed(title=f"{modename} Servers", color=0x00ff00))
        for region in servers[mode]:
            embeds[i].add_field(name=region, value=join.join(servers[mode][region]), inline=True)
        i+=1
    if embeds == []:
        await interaction.response.send_message(f'Sorry, we have no {region} {mode} servers',ephemeral=True)
    else:
        await interaction.response.send_message('Here you go:',ephemeral=True,embeds=embeds)


@bot.tree.command(name='server-availability',guild=discord.Object(id=serverID),description='shows when servers are in use on given day')
@app_commands.describe(date='The date you wish to see (yyyy/mm/dd)',timezone='The timezone you\'re in')
@app_commands.choices(timezone=[
        Choice(name='PST', value='US/Pacific'),
        Choice(name='CST', value='CST6CDT'),
        Choice(name='MST', value='MST'),
        Choice(name='EST', value='EST'),
        Choice(name='UTC', value='UTC'),
        Choice(name='GB', value='GB'),
        Choice(name='CET', value='CET'),
        Choice(name='AWST', value='Australia/West'),
        ])
async def serverAvailability(interaction:discord.Interaction,date:str,timezone:Choice[str]):

    hourDif = roundToQuarter((time.mktime(datetime.datetime.now(pytz.timezone(timezone.value)).timetuple()) - time.mktime(datetime.datetime.now().timetuple()))/3600)
    mins = int(60*(hourDif%1))
    hours = int(hourDif)
    
    day = date.split('/')
    if hours > 0:
        day = datetime.datetime(int(day[0]),int(day[1]),int(day[2])-1,24-hours,59-mins).timetuple()    
    else:
        day = datetime.datetime(int(day[0]),int(day[1]),int(day[2]),-hours,mins).timetuple()

    timestamp = int(time.mktime(day))
  
    calendar = json.loads(requests.get(config['API_URL']+'/getCalendar/%s'%timestamp).content)
    calendarImg = CalendarMaker.createCalendar(calendar,hourDif)
    await interaction.response.send_message('Here is the server availability for: '+date+f' ({timezone.name})',file=discord.File(calendarImg))



@bot.tree.command(name='refresh-config',guild=discord.Object(id=serverID),description='check config for changes')
async def refreshConfig(interaction:discord.Interaction):
    global config
    if not hasRole(interaction.user,'Mods'):
        await interaction.response.send_message('You are not allowed to use this',ephemeral=True)
    else:
        fConfig = open(configPath,)
        config = json.load(fConfig)['test1Local']
        requests.get(config['API_URL']+'/refreshConfig')
        await interaction.response.send_message('Done',ephemeral=True)


@bot.command()
async def deletePost(ctx,event_id):
    if  ctx.author.id == 1190619924016672808 or hasRole(ctx.author,'Mods'):
        try:
            await ctx.message.delete()
            event = getEvent(int(event_id))
            channel = bot.get_channel(config['EVENT_CHANNEL'])
            msg = await channel.fetch_message(event['posted'])
            await msg.delete()
        except:
            print('No post to be deleted',flush=True)

@bot.command()
async def changePost(ctx, event_id):
    if  ctx.author.id == 1190619924016672808 or hasRole(ctx.author,'Mods'):
        event = getEvent(int(event_id))
        embed = createEventEmbed(event)
        channel = bot.get_channel(config['EVENT_CHANNEL'])
        try:
            await ctx.message.delete()
            msg = await channel.fetch_message(event['posted'])
            await msg.edit(embed=embed)
        except:
            try:
                ping = config['PING_ROLES'][event['mode']]
            except:
                ping = '<@377204187089338373> needs to create a ping role for this mode'
            view = postedEvent(event['ID'])
            msg = await channel.send(f'<@&{ping}>',embed=embed,view=view)
            view.msg = msg
            await updateEvent(event['ID'],'posted',msg.id)          
    
@bot.event###################################### THIS ALLOWS COMMAND TO BE CALLED BY WEBHOOK ##############################################################
async def on_message(message): 
    # Manually get the invocation context from the message
    ctx = await bot.get_context(message)

    # Verify that the context has a command and can be used
    if ctx.valid:
        # Invoke the command using the earlier defined bot/client/command
        await bot.invoke(ctx)   
    


bot.run(config['DISCORD_TOKEN'])
