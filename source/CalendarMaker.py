from PIL import Image, ImageDraw, ImageFont
import datetime
from datetime import datetime
import time
import sys

path = sys.path[0]
event = Image.open(path+r'/event.png') 


def addRow(img:Image,serverName):
    
    row = Image.open(path+r"/calendarRow.png")
    
    draw = ImageDraw.Draw(row)
    font = ImageFont.truetype("DejaVuSans.ttf", 35)
    position = (10, 30)
    text_color = (0,0,0)  

    draw.text(position, serverName, fill=text_color, font=font)
    
    
    
    new_width = max(img.width, row.width)
    new_height = img.height + row.height

    calendarimg = Image.new("RGB", (new_width, new_height))

    calendarimg.paste(img, (0, 0))

    calendarimg.paste(row, (0, img.height))

    return calendarimg




def px(time):
    return int(time*(250/7200))


def pasteEvent(calendar,start,end,serverNum,hourdif):
    
    global event

    height = 100
    print(hourdif,flush=True)
    timeStart = (start+hourdif*3600)%86400
    timeEnd = (end+hourdif*3600)%86400
    if timeEnd < timeStart:
        timeEnd += 86400

    startPos = 500 + px(timeStart)
    startHeight = 50 + (serverNum)*100
    length = px(timeEnd-timeStart)
    print(length,height,startPos,startHeight)
    event = event.resize((length,height))
    calendar.paste(event, (startPos,startHeight))
    return calendar
    

def createCalendar(calendar,hourdif):
   
    calendarimg = Image.open(path+r"/calendarHeader.png")
    i=0
    for server in calendar:
        calendarimg = addRow(calendarimg,server)

        for event in calendar[server]:
            calendarimg = pasteEvent(calendarimg,event[0],event[1],i,hourdif)
        i += 1
    calendarimg.save(path+r'calendar.png')
    return path+r'calendar.png'







